from datetime import datetime


def log_func(func):
    def inner(*args, **kwargs):
        print(f"Calling {func.__name__} function.")
        print("Time of call: ", datetime.now())
        result = func(*args, **kwargs)
        return result

    return inner


def verify_args_int(func):
    def inner(*args):
        for item in args:
            if not isinstance(item, int):
                raise TypeError(f"{item} is not a integer.")

        res = func(*args)
        return res

    return inner


def verify_args(instance_type):
    def wrapper(func):
        def inner(*args):
            for item in args:
                if not isinstance(item, instance_type):
                    error_msg = f"{item} is not a type of {instance_type}."
                    raise TypeError(error_msg)

            res = func(*args)
            return res

        return inner
    return wrapper


@log_func
def fun1(a, b):
    print("fun1")
    return a + b


@log_func
@verify_args(int)
def fun2(a, b, c, d=50):
    print("fun2")
    return a + b + c + d


@log_func
def fun3(list1, list2):
    print("fun3")
    return list1.extend(list2)


# inner_obj = log_func(fun1)
# res = inner_obj(10, 20)
#
#
# wrapper_obj = verify_args(int)
# inner_obj_of_verify = wrapper_obj(log_func)
# inner_obj_of_log = inner_obj(fun2)
# result = inner_obj_of_log(10, 20, 30, 40)

# print(fun1("foo", "bar"))
# print(fun1(10, 20))
# print(fun2(10, 20, 30))
# print(fun3([1, 2, 3], [4, 5, 6]))


l1 = [1, 2, 3]
l2 = l1

l2.append(4)
print(l1, id(l1))
print(l2, id(l2))

l3 = l1.copy()
l4 = [i for i in l1]
print(l3, id(l3))
print(l4, id(l4))


import copy

l5 = copy.deepcopy(l1)
copy.copy(l1)


# Slicing - list, tuple, strings
l1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
l2 = l1[::-1]
print(l2)

text = "Hello World"
print(text[::-1])
