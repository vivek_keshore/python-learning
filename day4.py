# int, float, boolean, string, tuple - Immutable Datatypes
# list, dict, set - Mutable Datatypes

# Dictionary and set

# Sets

# List and Tuple - Ordered Datatype - Indexed

list1 = [1, 2, 3, 4, 5]
set1 = {1, 3, 2, 4, 5, 6, 8, 7, 9, 10, 1, 10, 9}  # Unordered, not indexed, no duplicates

# Only insert immutable types in set
set1 = {1, 2, 3, 'a', (1, 2, 3), 3.01, True, False, None}
print(set1)

# Mutability - Immutability - Hashability
# Set only takes immutable dataypes as items.

set1.add(100)
set1.add(101)

list1 = []
tuple1 = ()
set1 = set()  # Empty set
dict1 = {}  # empty dict

set1.add(100)
set1.add(101)
set1.add(102)
set1.add(103)

set1 = {1, 3, 2, 4, 5, 6, 8, 7, 9, 10, 1, 10, 99, 9}
set2 = {5, 6, 7, 12, 20}
set3 = {1, 2, 3, 99}

print(set1 - set2)  # Difference
print(set1.difference(set2))

print('Two ops ', set1.union(set2).intersection(set3))
print(set1.intersection(set2))

# Dictionary

dict1 = {}  # Empty dict
dict1 = {1.0: "one", 2.0: "two", 3.0: "three", 4.0: "four", 5.0: "five", 1.0: "One"}
# Key as number, value as spelling

print(dict1[1.0])  # Key inside square brackets

dict1[1.0] = "ONE"
print(dict1[1.0])  # Key inside square brackets


# till 3.5 dict was unordered. OrderedDict is ordered dict.
# 3.6 onwards dict is ordered. OrderedDict

del dict1[1.0]
dict1.pop(2.0)

dict1[6.0] = "Six"
print(dict1)

print(dict1.keys())
print(dict1.values())
print(list(dict1.values()))

print(list(dict1.items()))  # [(key, value), (key, value)]

dict2 = {"one": 1, "two": 2, "three": 3, "four": 4, "five": 5}

dict1.update(dict2)
print(dict1)
