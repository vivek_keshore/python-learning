# This is a program to convert temperature Celcius to Fahrenheit
# Rules for variable names
# 1. No spaces - Snake Case
# 2. No special characters - except underscore
# 3. No keywords - if, for, while, class, def, else, elif
# 4. No numbers at the beginning - associate1, associate2
# 5. No camel case - AssociateName except class names
# 6. No constants - ALL_CAPS

PI = 3.14159
AVOGADRO_NUMBER = 6.022140857e23

temp_in_celcius = float(input("Please enter the temperature in Celsius: "))  # Returns in string form
temp_in_fahrenheit = (temp_in_celcius * (9 / 5)) + 32
output = f"{temp_in_celcius} degree Celsius in Fahrenheit is {temp_in_fahrenheit} Fahrenheit"
print(output)

# Calculate the volume of sphere.
# Calculate the distance in miles from kilometers
# Calculate the area of a circle
# Calculate the area of a triangle
# Calculate the area of a rectangle
