while True:
    try:
        num = int(input("Please enter a number: "))
        if num < 0:
            raise ValueError("Number should be positive")

    except ValueError as ex:
        print("Number is invalid. Please enter a positive number.")
    except Exception as ex:
        print("Something went wrong", ex)
    else:
        print("Number is valid")
        break
