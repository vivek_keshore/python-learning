# Generators

list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

# for item in list1:
#     print(item)


def custom_range(num):  # Generator. All generators are iterators.
    i = 0
    while i <= num:
        yield i  # Pause, and returns to the caller
        i += 1  # Resume here


range_obj = custom_range(10)
val = next(range_obj)
print(val)
print(next(range_obj))
print(next(range_obj))
print(next(range_obj))



for item in custom_range(10):
    print(item)


dict1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}

for key, value in dict1.items():
    print(key, value)


def gen_example():
    yield 1  # Pause, return to caller
    print("yield 1")
    yield 2
    print("yield 2")
    yield 3
    print("yield 3")
    yield 4
    print("yield 4")
    yield 5
    print("yield 5")


for i in gen_example():
    print("print inside loop", i)


def factor_generator_method(num):
    for i in range(1, num + 1):
        if num % i == 0:
            yield i


for factor in factor_generator_method(100000):
    print(factor)

num = 100
factors = (
    factor
    for factor in range(1, num+1)
    if num % factor == 0
)
for factor in factors:
    print(factor)


r = range(1, 10)
print(type(r))


# Convert all loop programs to use generator or generator expression.
# Write a program to generate pi digits using generator
# 3.14159268956658465413546984656463846431321321846531321

# Write a program to generate fibonacci series using generator

# Write a program to generate prime numbers using generator


# Map
l1 = [1, 2, 3, 4, 5]
l2 = (6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
l3 = range(1, 6)


def add_list_items(num1, num2, num3):
    return num1 + num2 + num3

# print(add_list_items(l1, l2))


def sq_root(num):
    return num ** 0.5

# map -> function/method, and applied that method on every item of iterable obj.
sq_roots_gen = map(sq_root, l1)
for i in sq_roots_gen:
    print(i)

numbers = map(add_list_items, l1, l2, l3)
for i in numbers:
    print(i)


l2 = (6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
l4 = list(map(str, l2))

print(l4)


# Dunder methods or magic methods
