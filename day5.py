# # print(1)
# # print(2)
# # print(3)
# # print(4)
# # print(5)
# # print(6)
# # print(7)
# # print(8)
# # print(9)
# # print(10)
#
# # Iterable data types - list, tuple, set, dict, str
#
# # list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
# # for num in list1:
# #     print(num)
#
# for num in range(1, 11, 2):  # Start value, stop value, step value
#     print(num)  # Indentation
#
# list1 = [1, 3.14, True, "Vivek"]
# for item in list1:
#     print(item)
#
# for char in "vivek":
#     print(char)
#
# for i, char in enumerate("vivek"):
#     print(i, char)
#
# for num in range(1, 11):
#     print(f"3 x {num} = {3 * num}")
#
x = 10
1, 2, 4, 5, 10, 20, 25, 50, 100
for num in range(1, x + 1):
    if x % num == 0:  # Modulus operator
        print(num)  # Indentation is important

    print("Hello")

list2 = ["Rina", "Maths", "20", "Vivek", "English", "40", 876]
#
# for item1 in range(len(list2)):
#     print(f"The item value {item1} in the list is {list2[item1]}")

x = 378
list_of_factors = [1, 2, 5, 9, 278, 8, 2, 9, 5, 6, 7, 13]
# Which are the valid factors of x from the given list.

for num in list_of_factors:
    if x % num == 0:
        print(f"Valid Factor - {num}")
