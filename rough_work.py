var1 = 30
print(var1)

var2 = 20 if var1 == 10 else 30
print(var2)

var3 = 20 if var1 == 10 else 30 if var1 == 20 else 40
print(var3)

set1 = {1, 2, 3, 4, 5}
dict1 = {1: 1, 2: 4, 3: 9, 4: 16, 5: 25}

list1 = [0, 1, 2, 3, 4, 5]
list2 = [0, 1, 2, 3, 4, 5]

print(all(list1))  # 0 and 1 and 2 and 3 and 4 and 5
print(any(list1))  # 0 or 1 or 2 or 3 or 4 or 5
