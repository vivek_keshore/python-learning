class Button:  # Abstract base class
	def __init__(self, name):
		self.name = name

	def press(self):  # Every derived class must implement this method
		raise NotImplementedError


class FloorButton(Button):
	def press(self, floor_no):
		print(f"Floor button pressed - {floor_no}")
		return floor_no


class AlarmButton(Button):
	def press(self):
		print("Alarm button pressed")


class Door:
	def __init__(self):
		self.is_open = False

	def open(self):
		print("door opened")
		self.is_open = True

	def close(self):
		print("door closed")
		self.is_open = False


class Lift:
	def __init__(self):
		self.capacity = 0
		self.weight = 0.0
		self.floor_buttons = [
			FloorButton(0), FloorButton(1), FloorButton(2), FloorButton(3), FloorButton(4), FloorButton(5)
		]
		self.alarm_button = AlarmButton("Alarm")
		self.door = Door()

	def move(self, floor_num):
		print(f"Moving to floor {floor_num}")
		self.door.open()
		print(f"Arrived at floor {floor_num}")
		self.door.close()


lift = Lift()
floor_num = lift.floor_buttons[0].press(0)
lift.move(floor_num)

floor_num = lift.floor_buttons[1].press(1)
lift.move(floor_num)

floor_num = lift.floor_buttons[4].press(4)
lift.move(floor_num)
