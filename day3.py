# List Mutable
list_of_numbers = [1, 2, 3.14, 4, 3.14, 3.14, "Vivek"]  # Declared
# Index Value      0  1   2    3     4
# Index Value     -5  -4  -3  -2    -1
print(list_of_numbers)
print(list_of_numbers[-1])
print(list_of_numbers[4])
print(len(list_of_numbers))


list_of_numbers.append("Shreyas")
print(list_of_numbers)
print(len(list_of_numbers))

list_of_numbers.append(10)
print(list_of_numbers)
print(list_of_numbers[6])

list_of_numbers.pop()
list_of_numbers.pop()
list_of_numbers.pop()
print(list_of_numbers)
# [1, 2, 11, 3.14, 4]

list_of_numbers.insert(2, "Vivek")
print(list_of_numbers)

list_of_numbers.pop(2)
print(list_of_numbers)

list_of_numbers.remove(3.14)
print(list_of_numbers)
list_of_numbers.remove(3.14)
print(list_of_numbers)

list_of_numbers[1] = 100
print(list_of_numbers)

list_of_chars = ['a', 'b', 'c', 'd']

combined_list = list_of_numbers + list_of_chars
print(combined_list)
list_of_numbers.extend(list_of_chars)
print(list_of_numbers)

index_of_c = list_of_numbers.index("c")
print(f'index of c is {index_of_c}')

list_of_numbers.reverse()
print(list_of_numbers)

tuple_of_numbers = (1, 8, 5, 10, 47)
print(tuple_of_numbers)  # Immutable

print(tuple_of_numbers[3])
# tuple_of_numbers[3] = 1000

items = (1, 2, [3, 4, 5])
print(items[2])
items[2].append(4)
print(items)

my_list = [1, 2]
my_list.reverse()
c_list = my_list.copy()
print("Copied list", c_list)
