# Sample Output
# Alice - 95%
# Bob - 90%
# Charlie - 85%
# David - 80%

#  {"Kate": [90, 80, 49, 70, 89], "Bob": [90, 80, 49, 70, 89], "Alice": [90, 80, 49, 70, 89], "David": [90, 80, 49, 70, 89]}


def calculate_percentage(score_list: list) -> float:
    total_score = 0
    for score in score_list:
        total_score += score

    return total_score / len(score_list)


def read_file(file_name: str) -> dict:
    with open(file_name, "r") as students_score_file:
        student_score_map = {}

        for i, line in enumerate(students_score_file):
            if i == 0:
                continue

            student_name, _, student_score = line.strip().split(",")  # Unpacking

            student_scores = student_score_map.get(student_name, [])
            student_scores.append(int(student_score))
            student_score_map[student_name] = student_scores

        students_score_file.close()
        return student_score_map


score_map = read_file("students_score.csv")
print(score_map)


for student_name, student_scores in score_map.items():
    percentage = calculate_percentage(student_scores)
    print(f"{student_name} - {percentage}%")

# Complete to calculate percentage method

# Write another program, to find the topper in each subject.
# Find the highest and lowest scorer in each subject.
