input_file = open("input.csv", "r")
students_score_file = open("students_score.csv", "r")

# lines = input_file.readlines()  # List of each line in the file
#
# for line in lines:
#     print(line)

# Read -> str -> reads entire data in one go
# Readlines -> list -> reads each line in the file and returns a list of lines -> reads entire data in one go

for line in input_file:
    print(line)

input_file.close()
students_score_file.close()

# Sample Output
# Alice - 95%
# Bob - 90%
# Charlie - 85%
# David - 80%
