def return_hello():
    print("Inside return_hello")
    return "Hello World"


def foo():
    try:
        input_file = open("input.csv", "r")
        for line in input_file:
            x, y, z = line.strip().split(",")  # can give error.

        return return_hello()
    except FileNotFoundError as ex:
        print("Please enter the correct name of the file.")
    except TypeError as ex:
        pass
    except ValueError as ex:
        pass

    except Exception as e:
        pass

    finally:
        input_file.close()
        print("inside Finally")


foo_output = foo()
print(foo_output)


# Example of *args and **kwargs
def foo(*args, **kwargs):
    print(args)
    print(kwargs)


def foobar(x, y, z):
    print(x, y, z)

foobar(2, 4, 6)
foo([2, 4, 6])
