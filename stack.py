class Node:
	def __init__(self, item):
		self.item = item
		self.next = None

	def __str__(self):
		return f'Item Value - {self.item}'


class Stack:
	def __init__(self):
		self.top = None

	def __contains__(self, item):
		temp = self.top
		while temp is not None:
			if temp.item == item:
				return True
			temp = temp.next
		return False

	# def __add__(self, *args):


	def __str__(self):
		temp = self.top
		items = []
		while temp is not None:
			items.append(str(temp))
			temp = temp.next
		return " -> ".join(items)

	def push(self, item):
		node = Node(item)
		temp = self.top
		node.next = temp
		self.top = node

	def pop(self):
		if self.top is None:
			raise ValueError("Stack is empty")
		temp = self.top
		self.top = temp.next
		return temp.item

	def is_empty(self):
		return self.top is None

	def peek(self):
		return self.top


class A:
	pass

a = A()

stack = Stack()
stack2 = Stack()
stack.push(10)
stack.push(28)
stack.push(30)
stack.push(67)
stack.push(54)
stack.push(36)
# stack.push(a)

print(28 in stack)
print(stack)

for soldier in soldiers_stack:
	soldier.attack()

# 10 <- 28 <- 30 <- 67 <- 54 <- 36

stack3 = stack1 + stack2
