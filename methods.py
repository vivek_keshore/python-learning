# Abstract, Instance, Class, Static


class Vehicle:  # Abstract base class
	def brake(self):
		raise NotImplementedError

	def accelerate(self):
		raise NotImplementedError


class Car(Vehicle):
	brand = "Honda"

	def __init__(self, color):
		self.speed = 0
		self.color = color

	@classmethod
	def change_brand(cls, brand):
		cls.brand = brand
		
	@classmethod
	def create_car(cls, color):
		return cls(color)

	@staticmethod
	def vehicle_type():
		return "Car"

	@staticmethod
	def total_capacity(no_of_passengers):
		return no_of_passengers + 1

	def brake(self):
		print("Car brake")
		if self.speed > 10:
			self.speed -= 10
		else:
			self.speed = 0

	def accelerate(self):
		print("Car accelerate")
		self.speed += 10


class Bike(Vehicle):
	def brake(self):
		print("Bike brake")

	def accelerate(self):
		print("Bike accelerate")


bike1 = Bike()
bike1.accelerate()  # Instance Method
bike1.brake()  # Abstraction

bike2 = Bike()
bike2.accelerate()  # Instance Method

car1 = Car("Red")
car2 = Car("White")

Car.change_brand("Toyota")  # Class Method
print(car1.brand)
print(car2.brand)

car1.change_brand("BMW")  # Class Method
print(car1.brand)
print(car2.brand)
car1.accelerate()
car1.accelerate()
car1.accelerate()
car2.accelerate()

print(car1.speed)
print(car2.speed)


class IOOperation:
	def save(self):
		raise NotImplementedError


class FileOperation(IOOperation):
	def save(self):
		print("File saved")


class DatabaseOperation(IOOperation):
	def save(self):
		print("Database saved")


class NetworkOperation(IOOperation):
	def save(self):
		print("Network saved")


car3 = Car.create_car("Black")
car4 = Car.create_car("Blue")
car5 = Car.create_car("Green")

print(car3.vehicle_type())
print(Car.vehicle_type())
print(car4.total_capacity(5))
