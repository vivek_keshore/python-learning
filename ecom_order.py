from enum import Enum
from uuid import uuid4

# cart
# CartProduct
# user
# buyer
# seller
# product
# categories
# order
# Payment
# address
# shipping

class CategoryEnum(Enum):
	ELECTRONICS = "Electronics"
	BOOKS = "Books"
	CLOTHING = "Clothing"
	TOYS = "Toys"
	GROCERIES = "Groceries"


class Product:
	def __init__(self, name, price, category):
		self.name = name
		self.price = price
		self.category = category
		self.warranty = 1


p1 = Product("iPhone", 1000, CategoryEnum.ELECTRONICS)
p2 = Product("Hitchhikers Guide", 500, CategoryEnum.BOOKS)
p3 = Product("T-Shirt", 300, CategoryEnum.CLOTHING)
p4 = Product("T-Shirt", 600, CategoryEnum.CLOTHING)


class CartProduct:
	def __init__(self, product, quantity):
		self.product = product
		self.quantity = quantity
		self.amount = product.price * quantity

	def __str__(self):
		return f"Name = {self.product.name} and Qty = {self.quantity} and Total {self.amount}"


class Order:
	def __init__(self, shipping_address, billing_address, cart_products):
		self.order_id = uuid4()
		self.shipping_address = shipping_address
		self.billing_address = billing_address
		self.cart_products = cart_products
		self.payment_status = False

	def pay(self, payment):
		pass

	def update_payment_status(self):
		pass

	def cancel_order(self):
		pass


class Cart:
	def __init__(self):
		self.cart_products = []
		self.total_amount = 0

	def __str__(self):
		cart_products = [str(cart_product) for cart_product in self.cart_products]
		return f"Cart: {cart_products}, Total Amount: {self.total_amount}"

	def add_product(self, product, quantity):
		cart_product = CartProduct(product, quantity)
		self.cart_products.append(cart_product)
		self.total_amount += cart_product.amount

	def remove_product(self, cart_product, quantity):
		if cart_product.quantity < quantity:
			raise ValueError("Reducing quantity more than the quantity in cart")

		cart_product.quantity -= quantity
		cart_product.amount -= cart_product.product.price * quantity
		# TODO: implement update cart amount action.
		# TODO: Remove the product from cart if updated quantity reached 0
		cart.total_amount -= cart_product.product.price * quantity

	def checkout(self):
		# Todo: order object creation


cart = Cart()
cart.add_product(p1, 1)
cart.add_product(p3, 3)
cart.add_product(p4, 2)
print(cart)

cart.remove_product(cart.cart_products[1].name, 2)
print(cart)

order = cart.checkout()
