import random

for i in range(10):
    print(i)
    if i == 5:
        flag = True
        break

# If the break is executed, then else will not be executed.
else:  # If the loop terminates properly, then else is executed.
    print("Loop terminated properly")


# Given a number, check if it is prime or not.

num = 101
for i in range(2, (num // 2) + 1):
    if num % i == 0:
        print(f"Factor {i} | Number Not Prime")
        break
else:
    print("Number is Prime")

i = 0
while True:
    print(i)
    i += 1  # i = i + 1, i++
    if i == 10:
        break
else:
    print("Loop terminated properly")


# Guessing game
# 1 - 1000
# Maximum 8 guesses are needed to correctly guess
# any number between 1 - 10000

while True:
    print("Starting a new game ...")
    num = random.randrange(1, 1001)
    no_of_guesses = 0
    while no_of_guesses < 2:
        guessed_num = int(input("Guess the number: "))
        if num == guessed_num:
            print("You guessed it right")
            break
        elif num > guessed_num:
            print("Guess a higher number")
        else:
            print("Guess a lower number")

        no_of_guesses += 1

    else:
        print(f"You lost the game. the number was {num}")

    choice = input("Do you want to play again? (y/n): ")
    if choice == "n":
        break
    elif choice == "y":
        continue
    else:
        print("Invalid choice. Exiting the game")
        break


# Three random numbers from any range.
# Take two players - player_1 and player_2
# Take 3 numbers, if one player gets 2 or more than 2 bigger number,
# the player is winner.

player_1 = [10, 7, 5]  # 2 bigger numbers
player_2 = [8, 1, 6]  # 1 bigger numbers

# Rock paper scissor lizard spock

