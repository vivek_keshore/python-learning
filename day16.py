def my_sum(a, b):
    print("This method will never be called.")
    return a + b


def my_sum(a, b, *args):  # args acts like iterable
    print("Calling second method.")
    print(f"a = {a}, b = {b}, args = {args}")
    print(args, type(args))
    return sum(args)


input_1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# print(my_sum(*input_1))
#
#
# print(my_sum(1, 2, 3, 4))
# print(my_sum(1, 2))
# print(my_sum(1, 2, 3, 4, 5))
# print(my_sum(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15))


def my_sum_2(*args, c=10, d=20, e=50):  # args acts like iterable
    print("Calling second method.")
    total = 0
    total += sum(args)

    return total + c + d + e


input_1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
dict1 = {"c": 100, "d": 200, "e": 300}
dict2 = {"f": 100, "g": 200, "h": 300}
print(my_sum_2(*input_1, **dict1))


def add(a, b):
    return a + b


def multiply(a, b):
    return a * b


def operation(operation_func, a, b):
    return operation_func(a, b)


print(operation(multiply, 10, 20))
