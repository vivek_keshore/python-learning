d1 = {}  # Truthiness is False

d2 = {'a': 1}  # Truthiness is True

d2 = {(1, 2, 3): "Foobar"}

# int, float, string, bool, tuple - Key


# Nested dictionary
d3 = {
    "s": "Overwriting The value",
    "fruits": {
        "apple": 1,
        "banana": 2,
        "orange": 3,
    },
    "vegetables": {
        "carrot": 1,
        "tomato": 2,
        "onion": 3,
    },
}

fruits_dict = d3["fruits"]
# print(fruits_dict)

d4 = d3.get("Groceries", {1: 2, 3: 4})
d5 = d3.get("key1", {})
d6 = d3.get("key2", "Key Doesn't Exist")
d7 = d3.get("key3", (1, 2, 3))
# print(d4)
# print(d5)
# print(d6)
# print(d7)

text = "sdfhklajfkladjflkjadskljfasdkfjklasdhflkjasdhfkjsadhflkjadsasdkjlfhiuwqerlfkmjladskhfkjasdhfknkadshfk"

char_freq = {}
for char in text:
    char_freq[char] = char_freq.get(char, 0) + 1

print(char_freq)

char_freq.update(d3)
# print(char_freq)

d3["new-key"] = 1000

items = d3.items()
# print(items)

for key, value in d3.items():
    print(f"{key} = {value}")

for key in d3.keys():
    print(key)
for value in d3.values():
    print(value)

popped_value = d3.pop("s")
# del d3["s"]
# print(popped_value)

if "s" in d3:
    print("Key does not exists")
