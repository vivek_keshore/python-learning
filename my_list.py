# l1 = [1, 2, 3, 4]
# l1.append(5)
# l1.pop()
# len(l1)
# 10 in l1
# for item in l1:
# 	print(item)
#
# l2 = [5, 6, 7, 8]
#
# l3 = l1 + l2

class MyList:
	def __new__(cls, *args, **kwargs):

	def __init__(self):
		self.items = []

	def __contains__(self, item: object) -> bool:
		return item in self.items

	def __add__(self, other: "MyList") -> "MyList":
		self.items.extend(other.items)
		return self

	def __len__(self) -> int:
		return len(self.items)

	def __iter__(self) -> iter:
		yield from self.items

	def add_item(self, item: object) -> None:
		self.items.append(item)

	def remove_last_item(self) -> object:
		return self.items.pop()


my_list = MyList()

print(my_list)
print(my_list.items)
my_list.add_item(10)
my_list.add_item(20)
my_list.add_item(30)
my_list.add_item(40)
print(my_list.items)
my_list.remove_last_item()
my_list.remove_last_item()
print(my_list.items)

my_list2 = MyList()
my_list2.add_item(50)
my_list2.add_item(60)
#
my_list = my_list + my_list2
print(my_list.items)
print(len(my_list))
print(10 in my_list)
print(11 in my_list)

for i in my_list:
	print(i)


l1 = [1, 2, 3]

my_set = set()
my_set.add(l1)
my_dict = {l1: "foobar"}

# Instance, Class, Static, Abstract
# Singleton
# MRO
# SOLID
# Metaclass
# Class based generator and decorator
# Class & function based context manager
# __slots__
# Getter, setter, deleter properties
