list1 = [1, 2, 3]  # True
empty_list = []  # False

dict1 = {1: "one", 2: "two", 3: "three"}  # True
empty_dict = {}  # False

str1 = "Vivek"  # True
empty_str = ""  # False

x = 10  # True
y = 0  # False

flag = True


if list1:
    print("List is not empty")

if not False:
    print("List is empty")

# Truthiness
# not, or, and  --> Logical Operators

# "", [], {}, set(), 0, False, None --> False
# Everything else is True

# Not of True  -> False
# Not of False -> True

x = 10
y = 20
z = 0

if x > y and y > 0 and not z:
    print("Will not be executed")

if (x > y and y > 0 and not z) or (z or x):
    print("Will be executed???")


if (y > x or x > y) and x and y and not z:
    pass
# if (y > x or x > y or z) and True and True and True:

# or is lazy, and is greedy.

x = 10 and 0 and 10  # What is the value of x?
print(x)

y = 10 or 20 and 30  # What is the value of y?
print(y)

z = not 10 and 20 and 30 or 50  # What is the value of z?
print(z)

a = False and True
print(a)

# FizzBuzz
# Print numbers from 1 - 100
# Divisible by 3 - Fizz, 5 - Buzz, 3 and 5 - FizzBuzz,
# else print the number

for num in range(1, 101):
    if num % 3 == 0 and num % 5 == 0:
        print("FizzBuzz")
    elif num % 3 == 0:
        print("Fizz")
    elif num % 5 == 0:
        print("Buzz")
    else:
        print(num)

marks = [82, 76, 76, 45, 50, 76, 88, 63, 48, 61, 59, 85, 78, 67, 49, 64, 87, 87, 62, 65, 86, 80, 40, 67, 81, 65, 84, 85, 66, 88, 83, 56, 77, 74, 49, 82, 49, 56, 69, 79, 41, 60, 62, 84, 45, 45, 74, 78, 55, 54]
# < 50 - F
# 50 - 59 - E
# 60 - 69 - D
# 70 - 79 - C
# 80 - 89 - B
# 90 - 100 - A

# print in this format - Student 1 is having grade - B

for i, mark in enumerate(marks):  # Enumerate starts from 0
    if mark < 50:
        print(f"Student {i + 1} is having {mark}: grade - F")

    elif 50 <= mark < 60:  # mark >= 50 and mark < 60
        print(f"Student {i + 1} is having {mark}: grade - E")

    elif 60 <= mark < 70:
        print(f"Student {i + 1} is having {mark}: grade - D")

    elif 70 <= mark < 80:
        print(f"Student {i + 1} is having {mark}: grade - C")

    elif 80 <= mark < 90:
        print(f"Student {i + 1} is having {mark}: grade - B")

    else:
        print(f"Student {i + 1} is having {mark}: grade - A")
