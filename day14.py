# Comprehension

num = int(input("Enter a number: "))
factors = []
for factor in range(1, num+1):
    if num % factor == 0:
        factors.append(factor)
    else:
        factors.append(-1)

print(factors)

# List Comprehension
factors = [
    factor
    if num % factor == 0 else -1
    for factor in range(1, num+1)
]
print(factors)

# Generator Expression
factors = (
    factor
    if num % factor == 0 else -1
    for factor in range(1, num+1)
)
print(factors)

# Set Comprehension
factors = {
    factor
    if num % factor == 0 else -1
    for factor in range(1, num+1)
}
print(factors)


# Dict Comprehension
squares = {
    i: i*i for i in range(1, num+1)
}
print(squares)


# List comprehension to contain prime numbers from 1 till num.
def is_prime(n):
    for i in range(2, (n//2) + 1):
        if n % i == 0:
            return False

    return True


prime_numbers = [i for i in range(2, num) if is_prime(i)]
print(prime_numbers)

# Table of given n
table_of_n = [num * i for i in range(1, 11)]
print(table_of_n)


test_numbers = [7 % y != 0 for y in range(2, 7)]
print(test_numbers)

prime_number = [x for x in range(2, 20) if all(x % y != 0 for y in range(2, x))]
print(prime_number)


num = int(input("Enter number:"))
factors = []
for factor in range(1, num+1):
    if num % factor == 0:
        factors.append(factor)
        factors.append(factor * factor)
print(factors)
