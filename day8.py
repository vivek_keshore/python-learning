# Type Hinting
def check_primality(number: int):
    for i in range(2, (number // 2) + 1):
        if number % i == 0:
            print(f"Factor {i} | Number Not Prime - {number}")
            break
    else:
        print(f"Number is Prime - {number}")


def multiply(num1: int, num2: int) -> int:
    return num1 * num2


def count_chars_in_string(text: str, char: str) -> int:
    return text.count(char)


def print_pattern(pattern_char: str, pattern_rows: int):
    for i in range(1, pattern_rows + 1):
        print(pattern_char * i)


# for num in range(50):
#     check_primality(num)
#
#
# num = input()
# check_primality(num)

# output = multiply(10, 10)
# print(output)
#
# output = count_chars_in_string("dskjlfhasdkljfklasjdflkjfkljadsgfkjahriuehrklewfkjlqwnklcdnqlwkeuhlukqewkjn", "a")
# print(output)
#
# print_pattern("V", 10)


def get_volume_of_sphere(radius: float) -> float:
    return (4 / 3) * 3.14 * radius ** 3


def print_hello():
    print("Hello World")
